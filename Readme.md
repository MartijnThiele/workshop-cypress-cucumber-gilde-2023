# Workshop Cypress with Cucumber

## Preconditions
- Have nodeJS 14 or higher installed
- Have vscode installed and opened
- Create an empty folder to start your project in and open this folder in vscode
- Use 'ctrl+`' in vscode to start the integrated terminal (or select terminal from the view menu)

## 1. Installation

### Initialize new npm project, yes to all questions
- npm init -y

### Install cypress
- npm install cypress	

### Install esbuild preprocessor
- npm install @bahmutov/cypress-esbuild-preprocessor

### Install cucumber preprocessor:
- npm install @badeball/cypress-cucumber-preprocessor

### Install typescript
- npm install typescript

### Run cypress a first time to scaffold your project with the needed folders
- npx cypress open (allow firewall exception if prompted)
- configure setup e2e testing
- close cypress	

### Create tsconfig

- Create a file called tsconfig.json in your cypress folder 
- Add the following snippet to this tsconfig file:

```
{
    "compilerOptions": {
      "baseUrl": ".",
      "module": "commonjs",
      "moduleResolution": "node",
      "preserveConstEnums": true,
      "strict": true,
      "target": "es2017",
      "lib": ["ES2017", "dom"],
      "types": ["node"],
      "allowSyntheticDefaultImports": true
    },
    "supportFile": "./support/e2e.ts",
    "include": ["**/*.ts"],
}
```

### Tell cypress to use the preprocessor and where to find the feature files

- delete all the contents of your cypress.config.ts file
- add the following to your cypress.config.ts:

```
import { defineConfig } from "cypress";
import createBundler from "@bahmutov/cypress-esbuild-preprocessor";
import { addCucumberPreprocessorPlugin } from "@badeball/cypress-cucumber-preprocessor";
import createEsbuildPlugin from "@badeball/cypress-cucumber-preprocessor/esbuild";

async function setupNodeEvents(
  on: Cypress.PluginEvents,
  config: Cypress.PluginConfigOptions
): Promise<Cypress.PluginConfigOptions> {
  // This is required for the preprocessor to be able to generate JSON reports after each run, and more,
  await addCucumberPreprocessorPlugin(on, config);

  on(
    "file:preprocessor",
    createBundler({
      plugins: [createEsbuildPlugin(config)],
    })
  );

  // Make sure to return the config object as it might have been modified by the plugin.
  return config;
}

export default defineConfig({
  e2e: {
    specPattern: "**/*.feature",
    setupNodeEvents,
  },
});
```

### Tell cypress where to look for step definitions

- put the following snippet inside your package.json, after the scripts section:

```
  "cypress-cucumber-preprocessor": {
    "stepDefinitions": [
      "cypress/e2e/step-definitions/*.ts"
    ]
  },
```

## 2. Writing and running tests

### Create our first feature file

- create a folder called 'e2e' in your cypress folder
- create a folder called 'features' in your cypress/e2e folder and in it create a file called testShop.feature
- add the following snippet to this feature file to create our first scenario:

  ```
  Feature: Polteq testshop tests

  Scenario: Verify title
    Given I am on the polteq testshop homepage
    Then I should see Polteq Demo Testshop as the Title
  ```

### Try to run this first test:
- npx cypress run
- fails because steps have not been implemented yet

### Implement step definitions
- create a folder called 'step-definitions' in your cypress/e2e folder 
- create a file called testShop.ts in this new folder
- add the following snippet to this file: 

```
import { Given, When, Then } from "@badeball/cypress-cucumber-preprocessor";

Given("I am on the polteq testshop homepage", () => {
  cy.visit("https://testshop.polteq-testing.com/en/");
});
Then("I should see Polteq Demo Testshop as the Title", () => {
  cy.get("title").should('contain', 'Polteq Demo Testshop');
});
```

### Rerun these tests to check if they work
- npx cypress run

## 3. Tips and tricks

### Add cucumber plugin to vscode for better support of feature files and steps
- In the extensions menu (left toolbar, blocks icon) search for 'gherkin'
- Install the first search result 'Cucumber (Gherkin) full support'
- Add a folder called '.vscode' to the root of your project
- in this folder add a file called settings.json
- in this file, add the following snippet:
```
{
    "cucumberautocomplete.steps": [
        "./cypress/e2e/step-definitions/*.ts"
    ],
    "cucumberautocomplete.strictGherkinCompletion": false
}
```
- Restart vscode to load the newly installed plugin
- ctrl+click on a step in a feature file will now link to the step definition
- Steps that have not been implemented will now be underlined in yellow
- many more features described in the docs

### Use varibles in your step definitions

- The step that checks the title could be used in many other places if we make it more generic
- in our feature file, add single quotes around Polteq Demo Testshop
```
Then I should see 'Polteq Demo Testshop' as the Title
```
- in our step definition, change the then step to:
```
Then("I should see {string} as the Title", (title) => {
  cy.get("title").should('contain', title);
});
```
- Rerun the tests, they should still work

### Use a background for recurring steps

- Add the following test that checks the logo on the homepage:
```
Scenario: Verify logo
    Given I am on the polteq testshop homepage
    Then I should see the polteq logo on the page
```
- Add a background block to the top of the feature, after the feature name:
```
Background:
    Given I am on the polteq testshop homepage
```
- Remove the 'Given I am on the polteq testshop homepage' step from both scenarios
- Add a step definition that checks the logo:
```
Then("I should see the polteq logo on the page", (title) => {
  cy.get("[id='_desktop_logo']").should('be.visible');
});
```
- Rerun the tests, they should still work

### Using a teststore to share data between steps

- Add a simple test to our feature file to perform a search:
```
Scenario: Check search results
    When I search for notebook
    Then Results should include the word 'notebook'
```
- Add a teststore to the 'setupNodeEvents' function in our cypress.config.ts file:
```
const testStore : any = {};
  on('task', {
    pushValue({ name, value}) {
      testStore[name] = value
      return true
    },
    getValue(name) {
      return testStore[name] ?? null
    },
  });
```
- Add implementations for the steps in our new test:
```
When("I search for notebook", () => {
  cy.visit("https://testshop.polteq-testing.com/en/search?controller=search&s=notebook");
  cy.get('#products').find('#js-product-list').invoke('text').then((productlist) => {
    cy.task('pushValue', {
      name: 'productlist',
      value: productlist
    })
  });
});

Then("Results should include the word {string}", (include) => {
  cy.task("getValue", 'productlist').then((productlist) => {
    expect(productlist).to.contain(include);
  });
});
```
- run the tests to see if they work

### Use separate config files for different environments
- copy your 'cypress.config.ts' file, paste it in the root of the project and name it 'cypress.config.nl.ts'
- add an URL as env variable in the defineconfig section of both of our config files:
```
env: {
    URL: "https://testshop.polteq-testing.com/en/",
  },
```
- In our testshop.ts change the 'I am on the polteq testshop homepage' step to:
```
Given("I am on the polteq testshop homepage", () => {
  cy.visit(Cypress.env('URL'));
})
```
- Select which config file will be used by running 'npx cypress run --config-file ./cypress.config.nl.ts'


### Running scenarios based on tags
- Add the tag @smoketest to our first scenario:
```
@smoketest
  Scenario: Verify title
    Then I should see 'Polteq Demo Testshop' as the Title
```
- Run with the following command: npx cypress run --config-file ./cypress.config.nl.ts --env tags=@smoketest
- Only the tagged scenario is ran



