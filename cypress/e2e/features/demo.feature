Feature: Polteq testshop tests

  Background:
    Given I am on the polteq testshop homepage

  @smoketest
  Scenario: Verify title
    Then I should see 'Polteq Demo Testshop' as the Title

  Scenario: Verify logo
    Then I should see the polteq logo on the page

  Scenario: Check search results
    When I search for notebook
    Then Results should include the word 'notebook'
 