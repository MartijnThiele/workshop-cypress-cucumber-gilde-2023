import { Given, When, Then } from "@badeball/cypress-cucumber-preprocessor";

Given("I am on the polteq testshop homepage", () => {
  cy.visit(Cypress.env('URL'));
});

Then("I should see {string} as the Title", (title) => {
  cy.get("title").should('contain', title);
});

Then("I should see the polteq logo on the page", (title) => {
  cy.get("[id='_desktop_logo']").should('be.visible');
});

When("I search for notebook", () => {
  cy.visit("https://testshop.polteq-testing.com/en/search?controller=search&s=notebook");
  cy.get('#products').find('#js-product-list').invoke('text').then((productlist) => {
    cy.task('pushValue', {
      name: 'productlist',
      value: productlist
    })
  });
});

Then("Results should include the word {string}", (include) => {
  cy.task("getValue", 'productlist').then((productlist) => {
    expect(productlist).to.contain(include);
  });
});

